#pragma once
#include <cstdlib>
#include <iostream>

class Matrix
{
private:
	double** matr;
	int N;
public:
	Matrix(int N) {
		this->N = N;
		matr = new double* [N];
		for (int i = 0; i < N; i++) {
			matr[i] = new double[N];
		}
	}

	void creatRandomMatrix() {
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				matr[i][j] = rand();
			}
		}
	}

	void printMatrix() {
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				std::cout << matr[i][j] << " ";
			}
			std::cout << std::endl;
		}
	}

	~Matrix() {
		for (int i = 0; i < N; i++) {
			delete matr[i];
		}
		delete matr;
	}
};

